# Marvel

Repositório criado para o processo seletivo da softplan

  - React
  - Redux

# Como rodar
```sh
$ yarn install
$ yarn start
```
O servidor estará rodando na porta 3000

# Pipeline
  - code_quality - padroniza o código
  - test - Executa os testes
  - build - executa o build

qualquer dúvida criar uma issue ou enviar um e-mail para dafirsouza@gmail.com
