import React, { useState } from 'react'
import Modal from 'react-responsive-modal'
import Api from 'api'
import Loading from 'layout/loading'
import Card from 'components/card'
import { Link } from 'react-router-dom'

function Header () {
  const [modalOpen, setModal] = useState(false)
  const [searchText, changeText] = useState('')
  const [characters, setCharacters] = useState([])
  const [loading, setLoading] = useState(false)

  const handleInput = event => {
    const { value } = event.target
    changeText(() => value)
  }

  const getApi = async (name) => {
    try {
      setLoading(() => true)
      setModal(() => true)
      const response = await Api.get('characters', { name })
      setTimeout(() => {
        setCharacters(() => response.data.results)
        setLoading(
          () => false)
      }, 3000)
    } catch (error) {
      console.log(error.message)
    }
  }
  return (
    <>
      <div className='row'>
        <div className='header flex-12'>
          <div className='header--logo flex-1 flex-sm-3'>
            <Link to='/'>
              <img src='https://logodownload.org/wp-content/uploads/2017/05/marvel-logo.png' alt='logo marvel' />
            </Link>
          </div>
          <div className='header--search flex-11 flex-sm-9'>
            <input
              type='text' className='header--search-input flex-11 flex-sm-8'
              placeholder='Pesquise por um personagem ex: Spider-Man'
              onChange={handleInput} value={searchText}
              onKeyDown={(event) => {
                if (event.keyCode === 13) {
                  getApi(searchText)
                }
              }}
              data-testid='header-input'
            />
            <span className='header--search-button flex-1 flex-sm-4' onClick={() => getApi(searchText)} data-testid='btn-search'>Pesquisar</span>
          </div>
        </div>
      </div>
      <Modal open={modalOpen} onClose={() => setModal(() => false)} center classNames={{ modal: 'modalSearch' }}>
        <div className='row modalSearch--row'>
          {!loading &&
            <>
              {characters.length === 0 && <h1>Não foram encontrado Resultados</h1>}
              {characters.length !== 0 &&
                <>
                  <h1>Resultados encontrados: </h1>
                  {characters.map(_c =>
                    <Card key={_c.id} character={_c} />)}
                </>}
            </>}
          {loading &&
            <>
              <Loading />
            </>}
        </div>
      </Modal>
    </>
  )
}

export default Header
