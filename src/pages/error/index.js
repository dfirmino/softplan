/* eslint-disable react/react-in-jsx-scope */
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ErrorPage from 'pages/error/errorPage'

class ErrorBoundary extends PureComponent {
    state = {
      hasError: false
    }

    static getDerivedStateFromError () {
      return { hasError: true }
    }

    componentDidCatch (error, info) {
      console.log(error.message)
      console.log(info.componentStack)
    }

    render () {
      if (this.state.hasError) return <ErrorPage />
      return this.props.children
    }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired
}

export default ErrorBoundary
