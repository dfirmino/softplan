import React from 'react'

function ErrorPage () {
  return (
    <div className='errorPage'>
      <div className='errorPage--header'>
        <h1>Aconteceu um Erro</h1>
      </div>
      <div className='errorPage--body'>
        <img src='https://a-static.mlcdn.com.br/618x463/painel-festa-deadpool-150x100cm-x4adesivos/x4adesivos-tray/8646/16b4f127bb53fd233a4227140d9bf8e5.jpg' alt='deadpool erro' />
      </div>
    </div>
  )
}

export default ErrorPage
