import React from 'react'
import { Link } from 'react-router-dom'
import Header from 'layout/header'
function NotFound () {
  return (
    <>
      <Header />
      <div className='notFound'>
        <div className='notFound--header'>
          <div className='row'>
            <h1 className='flex-12'>Ops...</h1>
            <h2 className='flex-12'>Página não encontrada</h2>
          </div>
        </div>
        <div className='notFound--body'>
          <div className='row'>
            <img src='https://www.jing.fm/clipimg/full/88-882565_deadpool-clipart-cool-transparent-background-deadpool-2-png.png' alt='deadpool não encontrado' />
          </div>
          <div className='row'>
            <Link to='/'>
              <span>← Voltar para a home</span>
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}

export default NotFound
