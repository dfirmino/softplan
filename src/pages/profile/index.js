import React, { useState, useEffect } from 'react'
import Header from 'layout/header'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import { editCharacter } from 'store/actions/charactersActions'

function Profile ({ match: { params }, history }) {
  const [url, setUrl] = useState('')
  const [disable, setDisable] = useState(true)
  const { characters } = useSelector(state => state.characters)
  let [selectedCharacter] = characters.filter(_c => Number(_c.id) === Number(params.id))
  const dispatch = useDispatch()

  if (!selectedCharacter) history.push('/')

  useEffect(() => {
    let finalUrl = 'https://www.publicdomainpictures.net/pictures/280000/nahled/not-found-image-15383864787lu.jpg'
    if (selectedCharacter && selectedCharacter.thumbnail.path) finalUrl = `${selectedCharacter.thumbnail.path}/landscape_medium.${selectedCharacter.thumbnail.extension}`
    setUrl(() => finalUrl)
  }, [selectedCharacter])

  const handleChange = event => {
    const { id, value } = event.currentTarget
    selectedCharacter = {
      ...selectedCharacter,
      [id]: value
    }
  }

  const save = () => {
    dispatch(editCharacter(selectedCharacter))
    setDisable(true)
  }

  return (
    <div className='profile'>
      <Header />
      <div className='profile--body'>
        {selectedCharacter &&
          <>
            <div className='row' style={{ justifyContent: 'flex-end' }}>
              {disable && <span className='btn-editar' onClick={() => setDisable(false)}>Editar</span>}
              {!disable && <span className='btn-salvar' onClick={save}>Salvar</span>}
            </div>
            <div className='row'>
              <img src={url} alt='foto personagem selecionado' />
            </div>
            <div className='row'>
              <span className='flex-3 label'> Nome </span>
              <input className='flex-8' disabled={disable} type='text' defaultValue={selectedCharacter.name} onChange={handleChange} id='name' />
            </div>
            <div className='row'>
              <span className='flex-3 label'> Descrição </span>
              <textarea className='flex-8' disabled={disable} defaultValue={selectedCharacter.description} onChange={handleChange} id='description' />
            </div>
            <div className='row'>
              <span className='flex-12 label'>Séries</span>
            </div>
            <div className='profile--body-series'>
              {selectedCharacter.series.items.map(_i =>
                <span key={_i.name}> {_i.name}</span>
              )}
            </div>
          </>}
      </div>
    </div>
  )
}

Profile.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object
}

export default Profile
