import React, { Suspense } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Loading from 'layout/loading'
const Home = React.lazy(() => import('pages/home/'))
const NotFound = React.lazy(() => import('pages/notFound'))
const Profile = React.lazy(() => import('pages/profile'))

function Routes () {
  return (
    <>
      <Suspense fallback={<Loading />}>
        <BrowserRouter>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/profile/:id' component={Profile} />
            <Route component={NotFound} />
          </Switch>
        </BrowserRouter>
      </Suspense>
    </>
  )
}

export default Routes
