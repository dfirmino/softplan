import React, { useState } from 'react'
import Header from 'layout/header'
import { useSelector } from 'react-redux'
import Card from 'components/card'

function Home () {
  const [textFilter, setText] = useState('')
  const [filtredLines, setFilteredLines] = useState([])
  const { characters } = useSelector(state => state.characters)

  const handleText = (event) => {
    const value = event.target.value
    setText(() => value)
  }

  const filter = () => {
    if (!textFilter) return setFilteredLines(() => [])
    const filteredCharactes = characters.filter(_c =>
      _c.name.toLowerCase()
        .indexOf(textFilter.toLowerCase()) !== -1
    )
    setFilteredLines(() => filteredCharactes)
  }
  return (
    <>
      <div className='home'>
        <Header />
        <div className='home--body'>
          {characters.length !== 0 &&
            <>
              <div className='row'>
                <div className='home--body-filter flex-12 flex-sm-12'>
                  <div className='home--body-filter-input flex-9 flex-sm-6'>
                    <input
                      className='flex-12' type='text'
                      value={textFilter}
                      placeholder='Pesquise por um personagem na sua lista'
                      onChange={handleText}
                      onKeyDown={(event) => {
                        if (event.keyCode === 13) {
                          filter()
                        }
                      }}
                    />
                  </div>
                  {filtredLines.length !== 0 &&
                    <div className='home--body-filter-clean flex-2 flex-sm-3' onClick={() => setFilteredLines(() => [])}>
                      <span>Limpar</span>
                    </div>}
                  <div className='home--body-filter-search flex-2 flex-sm-3'>
                    <span onClick={filter}>Pesquisar</span>
                  </div>
                </div>
              </div>
              <div className='row'>
                <div className='home--body-list'>
                  <h1>Meus Personagens</h1>
                  <div className='row'>
                    {filtredLines.length === 0 && characters.map(_c => <Card key={_c.id} character={_c} edit />)}
                    {filtredLines.length !== 0 && filtredLines.map(_c => <Card key={_c.id} character={_c} edit />)}
                  </div>
                </div>
              </div>
            </>}
          {characters.length === 0 &&
            <div className='home--body-empty'>
              <h1>Não foram encontrado personagens em sua lista</h1>
              <h2>Pesquise personagens na barra de pesquisa e personagens</h2>
              <img src='https://i.ibb.co/0c9zdnF/hiclipart-com.png' alt='deadpool nao encontrado persongens' />
            </div>}
        </div>
      </div>
    </>
  )
}

export default Home
