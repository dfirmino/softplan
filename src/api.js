class Api {
  constructor () {
    this.url = 'https://gateway.marvel.com/v1/public'
    this.public = '7c564ecdd112100b6f8a5eb5d7c4c75b'
    this.timestamp = '1586108980'
    this.hash = '98887fa8a2a92d055bf45c0510101d39'
  }

  async get (url, params) {
    let _p = ''
    if (params) _p = this.serialize(params)
    return fetch(`${this.url}/${url}?ts=${this.timestamp}&apikey=${this.public}&hash=${this.hash}&${_p}`).then(res => res.json())
  }

  serialize (obj) {
    var str = []
    for (var p in obj) {
      // eslint-disable-next-line no-prototype-builtins
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]))
      }
    }
    return str.join('&')
  }
}

export default new Api()
