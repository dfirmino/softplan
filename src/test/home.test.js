import React from 'react'
import configureMockStore from 'redux-mock-store'
import { render } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import Home from 'pages/home'
import INITIAL_STATE from './initiaStateFake'

const middlewares = []
const mockStore = configureMockStore(middlewares)
const store = mockStore({ ...INITIAL_STATE })

const rendercomponent = () => render(
  <Provider store={store}>
    <BrowserRouter>
      <Home />
    </BrowserRouter>
  </Provider>)

it('testing listing cards home', async () => {
  const { getByTestId } = rendercomponent()
  expect(!!getByTestId('Spider-Man')).toBe(true)
})
