import React from 'react'
import { render, fireEvent, waitFor } from '@testing-library/react'
import Header from 'layout/header'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from 'store/'
window.scroll = () => {}
jest.setTimeout(15000)

const rendercomponent = () => render(
  <Provider store={store}>
    <BrowserRouter>
      <Header />
    </BrowserRouter>
  </Provider>)

it('testing card listing and add', async () => {
  const { getByTestId, container } = rendercomponent()

  const input = getByTestId('header-input')
  fireEvent.change(input, { target: { value: 'Spider-Man' } })
  fireEvent.keyDown(input, { keyCode: 13 })

  await waitFor(() => {
    expect(!!getByTestId('Spider-Man')).toBe(true)
  }, { container, timeout: 10000 })

  const buttonAdd = getByTestId('btn-add')
  fireEvent.click(buttonAdd)
})

it('testing store', async () => {
  const { characters } = await store.getState().characters
  expect(characters.length).toBe(1)
})
