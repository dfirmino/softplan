import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { addCharacter, removeCharacter } from 'store/actions/charactersActions'
import { Link } from 'react-router-dom'

function Card ({ character, edit }) {
  const [photoUrl, setUrl] = useState('')
  const dispatch = useDispatch()
  const { characters } = useSelector(state => state.characters)

  useEffect(() => {
    let finalUrl = 'https://www.publicdomainpictures.net/pictures/280000/nahled/not-found-image-15383864787lu.jpg'
    if (character.thumbnail.path) finalUrl = `${character.thumbnail.path}/landscape_medium.${character.thumbnail.extension}`
    setUrl(() => finalUrl)
  }, [character])

  const addStore = (character) => {
    dispatch(addCharacter(character))
  }

  const removeStore = (character) => {
    dispatch(removeCharacter(character))
  }
  return (
    <>
      <div className='card flex-5 flex-sm-12' data-testid={character.name}>
        <div className='card--photo'>
          <img src={photoUrl} alt='foto personagem' />
        </div>
        <div className='card--name'>
          <span>{character.name}</span>
        </div>
        <div className='card--description'>
          <span>{character.description}</span>
        </div>
        {characters.filter(_c => _c.id === character.id).length === 0 &&
          <div className='card--add'>
            <span data-testid='btn-add' onClick={() => addStore(character)}>Adicionar</span>
          </div>}
        {characters.filter(_c => _c.id === character.id).length !== 0 &&
          <div className='card--remove'>
            <span data-testid='btn-remove' onClick={() => removeStore(character)}>Remover</span>
          </div>}
        {edit &&
          <div className='card--view'>
            <Link to={`/profile/${character.id}`}>Visualizar</Link>
          </div>}
      </div>
    </>
  )
}

Card.propTypes = {
  character: PropTypes.object.isRequired,
  edit: PropTypes.bool
}

export default Card
