import { ADD_CHARACTER, REMOVE_CHARACTER, EDIT_CHARACTER } from '../actions/charactersActions'

const INITIAL_STATE = {
  characters: []
}

function characters (state = INITIAL_STATE, action) {
  switch (action.type) {
    case ADD_CHARACTER:
      return { ...state, characters: [...state.characters, action.character] }
    case REMOVE_CHARACTER:
      return { ...state, characters: state.characters.filter(_c => _c.id !== action.character.id) }
    case EDIT_CHARACTER:
      return {
        ...state,
        characters: state.characters.map(_c => {
          if (Number(_c.id) === Number(action.character.id)) return action.character
          return _c
        })
      }
    default:
      return state
  }
}

export default characters
