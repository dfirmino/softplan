export const ADD_CHARACTER = 'ADD_CHARACTER'
export const REMOVE_CHARACTER = 'REMOVE_CHARACTER'
export const EDIT_CHARACTER = 'EDIT_CHARACTER'

export const addCharacter = (character) => {
  return {
    type: ADD_CHARACTER,
    character
  }
}

export const removeCharacter = (character) => {
  return {
    type: REMOVE_CHARACTER,
    character
  }
}

export const editCharacter = (character) => {
  return {
    type: EDIT_CHARACTER,
    character
  }
}
